#!/usr/bin/env python3

import p
import util

MONTH_SIZES = {
  1: lambda year: 31,
  2: lambda year: 29 if (((year % 400) == 0) or (((year % 100) != 0) and ((year % 4) == 0))) else 28,
  3: lambda year: 31,
  4: lambda year: 30,
  5: lambda year: 31,
  6: lambda year: 30,
  7: lambda year: 31,
  8: lambda year: 31,
  9: lambda year: 30,
  10: lambda year: 31,
  11: lambda year: 30,
  12: lambda year: 31,
}

# date is
# (
#   year,
#   month (1-12),
#   day (1-31),
#   day of week (1: mon, 7: sun),
# )
def dates(start=(1900, 1, 1, 1)):
  year, month, day, dow = start
  month_size = MONTH_SIZES[month](year)
  while True:
    yield (year, month, day, dow)
    dow += 1
    if dow >= 8:
      dow = 1
    day += 1
    if day > month_size:
      day = 1
      month += 1
      if month > 12:
        month = 1
        year += 1
      month_size = MONTH_SIZES[month](year)

@p.p
def main():
  count = 0
  for year, _, day, dow in dates():
    if year >= 1901:
      if year > 2000:
        break
      if (day == 1) and (dow == 7):
        count += 1
  print(count)

if __name__ == "__main__":
  p.main()
