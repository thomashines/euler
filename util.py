#!/usr/bin/env python3

import itertools

def factors(number):
  pfs = tuple(prime_factors(number))
  factors_set = set()
  for pf_count in range(1, len(pfs) + 1):
    for c in itertools.combinations(pfs, pf_count):
      factors_set.add(product(c))
  return tuple(sorted(factors_set))

def factors_count(number, window_size=2 ** 5):
  repeats = 0
  last = 0
  count = 1
  for pf in prime_factors(number, window_size=window_size):
    if pf == last:
      repeats += 1
    else:
      count *= repeats + 1
      last = pf
      repeats = 1
  count *= repeats + 1
  return count

def gcd(a, b):
  afs = prime_factors(a)
  bfs = prime_factors(b)
  af = 1
  bf = 1
  result = 1
  while af is not None and bf is not None:
    if af == bf:
      result *= af
      af = next(afs, None)
      bf = next(bfs, None)
    elif af > bf:
      bf = next(bfs, None)
    elif af < bf:
      af = next(afs, None)
  return result

def lcm(a, b):
  return (a * b) // gcd(a, b)

def prime_factors(number, window_size=2 ** 5):
  if number == 1:
    yield 1
  else:
    for prime in primes(number, window_size=window_size):
      while number % prime == 0:
        yield prime
        number = number // prime
      if number == 1:
        break

PRIME_CACHE = [2]
def primes(max_prime=None, window_size=2 ** 10):
  # Repeat cache
  for prime in PRIME_CACHE:

    # Stop
    if max_prime is not None and prime > max_prime:
      break

    yield prime

  # Window of sieve of Eratosthenes
  window_offset = 0
  not_prime_window = [False] * window_size

  # Find primes
  for number in itertools.count(start=(PRIME_CACHE[-1] + 1)):

    # Stop
    if max_prime is not None and number > max_prime:
      break

    # Move the sieve window
    if number >= window_offset + window_size:
      window_offset = (number // window_size) * window_size
      for i in range(window_size):
        not_prime_window[i] = False
      for prime in PRIME_CACHE:
        multiple = (window_offset // prime) * prime
        while multiple < window_offset + window_size:
          if multiple >= window_offset:
            not_prime_window[multiple - window_offset] = True
          multiple += prime

    # Test if prime
    if not not_prime_window[number - window_offset]:
      yield number

      # Add to the sieve
      PRIME_CACHE.append(number)
      for multiple in itertools.count(start=2):
        this_not_prime = number * multiple
        if this_not_prime >= window_offset + window_size:
          break
        not_prime_window[this_not_prime - window_offset] = True

def product(values):
  result = 1
  for value in values:
    result = result * value
  return result

def triangles():
  triangle = 0
  for step in itertools.count(start=1):
    triangle += step
    yield triangle
