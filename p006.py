#!/usr/bin/env python3

import p

@p.p
def main():
  print(sum(range(1, 101)) ** 2 - sum(map(lambda x: x ** 2, range(1, 101))))

if __name__ == "__main__":
  p.main()
