#!/usr/bin/env python3

import p
import util

@p.p
def main():
  divsums = {}
  amicables = set()
  for number in range(1, 10000):
    factors = util.factors(number)
    divsum = 1 + sum(factors[:-1])
    divsums[number] = divsum
    if divsum != number and divsums.get(divsum, 0) == number:
      amicables.add(number)
      amicables.add(divsum)
  print(sum(amicables))

if __name__ == "__main__":
  p.main()
