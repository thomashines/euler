#!/usr/bin/env python3

import p
import util

import math

@p.p
def main():
  print(math.factorial(40) // (math.factorial(20) * math.factorial(20)))

if __name__ == "__main__":
  p.main()
