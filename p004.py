#!/usr/bin/env python3

import p

@p.p
def main():
  q = {(999 * 999, 999, 999)}
  prod = None
  while q:
    top = max(q)
    q.remove(top)
    prod, a, b = top
    prod = str(prod)
    if prod == prod[::-1]:
      break
    aa = a - 1
    bb = b - 1
    q.add((a * bb, a, bb))
    q.add((aa * b, aa, b))
  print(prod)

if __name__ == "__main__":
  p.main()
