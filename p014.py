#!/usr/bin/env python3

import p
import util

CACHE = {1: 1}
def collatz(number):
  if number not in CACHE:
    next = None
    if number % 2 == 0:
      next = number // 2
    else:
      next = 3 * number + 1
    CACHE[number] = 1 + collatz(next)
  return CACHE[number]

@p.p
def main():
  best = 1
  best_length = 1
  for start in range(1, 1000000):
    length = collatz(start)
    if length > best_length:
      best = start
      best_length = length
  print(best)

if __name__ == "__main__":
  p.main()
