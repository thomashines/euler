#!/usr/bin/env python3

import p
import util

@p.p
def main():
  ps = 0
  for prime in util.primes(window_size=2 ** 13):
    ps += 1
    if ps == 10001:
      print(prime)
      break

if __name__ == "__main__":
  p.main()
