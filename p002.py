#!/usr/bin/env python3

import p

@p.p
def main():
  ans = 0
  a = 0
  b = 1
  while a + b < 4000000:
    c = a + b
    if c % 2 == 0:
      ans += c
    a = b
    b = c
  print(ans)

if __name__ == "__main__":
  p.main()
