#!/usr/bin/env python3

import p
import util

@p.p
def main():
  abundants = []
  ints = set(range(1, 20162))
  for number in range(1, 20162):
    factors = util.factors(number)
    divsum = 1 + sum(factors[:-1])
    if number < divsum < 20162:
      abundants.append(number)
      threshold = 20162 - number
      for abundant in abundants:
        if abundant <= threshold:
          ints.discard(abundant + number)
  print(sum(ints))

if __name__ == "__main__":
  p.main()

