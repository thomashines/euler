#!/usr/bin/env python3

import functools

import p
import util

@p.p
def main():
  print(functools.reduce(util.lcm, range(1, 21), 1))

if __name__ == "__main__":
  p.main()
