#!/usr/bin/env python3

import p
import util

import math

@p.p
def main():
  print(sum(map(int, str(math.factorial(100)))))

if __name__ == "__main__":
  p.main()
