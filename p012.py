#!/usr/bin/env python3

import p
import util

@p.p
def main():
  for triangle in util.triangles():
    factors_count = util.factors_count(triangle, window_size=2 ** 5)
    if factors_count >= 500:
      print(triangle)
      break

if __name__ == "__main__":
  p.main()
