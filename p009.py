#!/usr/bin/env python3

import p

@p.p
def main():
  for c in range(1, 1001):
    for b in range(1, 1001):
      a = 1000 - c - b
      if not (a < b < c) or (a <= 0) or (b <= 0) or (c <= 0) or a + b + c != 1000:
        continue
      if a * a + b * b == c * c:
        print(a * b * c)
        return

if __name__ == "__main__":
  p.main()
