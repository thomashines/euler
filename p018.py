#!/usr/bin/env python3

import p
import util

# import heapq

TRIANGLES = (
# """3
# 7 4
# 2 4 6
# 8 5 9 3""",
"""75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23""",
)

@p.p
def main():
  for triangle in TRIANGLES:
    rows = tuple(tuple(map(int, line.split(" "))) for line in triangle.split("\n"))

    # Lowest cost
    # # pq elements are
    # # (
    # #   total cost,
    # #   row,
    # #   col,
    # # )
    # pq = []
    # heapq.heappush(pq, (rows[0][0], 0, 0))
    # while pq:
    #   cost, row, col = heapq.heappop(pq)
    #   if row >= (len(rows) - 1):
    #     print(cost)
    #     break
    #   heapq.heappush(pq, (cost + rows[row + 1][col], row + 1, col))
    #   heapq.heappush(pq, (cost + rows[row + 1][col + 1], row + 1, col + 1))

    # Highest cost
    prior = rows[0]
    for row in rows[1:]:
      next = list(row)
      for index, prior_cost in enumerate(prior):
        next[index] = max(next[index], row[index] + prior_cost)
        next[index + 1] = max(next[index + 1], row[index + 1] + prior_cost)
      prior = next
    print(max(prior))


if __name__ == "__main__":
  p.main()
