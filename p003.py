#!/usr/bin/env python3

import p
import util

@p.p
def main():
  print(max(util.prime_factors(600851475143)))

if __name__ == "__main__":
  p.main()
