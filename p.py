#!/usr/bin/env python3

import timeit

puzzles = []
def p(puzzle):
  def time_puzzle():
    duration = timeit.timeit(lambda: puzzle(), number=1)
    print("#", duration * 1e3, "ms")
  puzzles.append(time_puzzle)
  return time_puzzle

def main():
  def solve_all():
    for number, puzzle in enumerate(puzzles, start=1):
      print("#", number)
      puzzle()
  duration = timeit.timeit(lambda: solve_all(), number=1)
  print("# all in", duration * 1e3, "ms")

if __name__ == "__main__":
  main()
