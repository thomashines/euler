#!/usr/bin/env python3

import p
import util

@p.p
def main():
  names = []
  with open("p022.in", "r") as infile:
    names.extend(name.strip("\"\r\n") for name in infile.read().split(","))
  names.sort()
  total = 0
  for position, name in enumerate(names, start=1):
    total += position * sum(ord(letter) - ord("A") + 1 for letter in name)
  print(total)

if __name__ == "__main__":
  p.main()
