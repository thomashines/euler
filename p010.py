#!/usr/bin/env python3

import p
import util

@p.p
def main():
  print(sum(util.primes(2000000, window_size=2 ** 21)))

if __name__ == "__main__":
  p.main()
