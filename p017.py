#!/usr/bin/env python3

import p
import util

SMALL = {
  0: "zero",
  1: "one",
  2: "two",
  3: "three",
  4: "four",
  5: "five",
  6: "six",
  7: "seven",
  8: "eight",
  9: "nine",
  10: "ten",
  11: "eleven",
  12: "twelve",
  13: "thirteen",
  14: "fourteen",
  15: "fifteen",
  16: "sixteen",
  17: "seventeen",
  18: "eighteen",
  19: "nineteen",
}
TIES = {
  20: "twenty",
  30: "thirty",
  40: "forty",
  50: "fifty",
  60: "sixty",
  70: "seventy",
  80: "eighty",
  90: "ninety",
}
BIGS = {
  100: "hundred",
  1000: "thousand",
}

def human(number):
  if number in SMALL:
    return SMALL[number]
  for big in reversed(sorted(BIGS.keys())):
    if number >= big:
      count = number // big
      as_str = human(count) + " " + BIGS[big]
      remainder = number - count * big
      if remainder > 0:
        as_str += " and " + human(remainder)
      return as_str
  for tie in reversed(sorted(TIES.keys())):
    if number == tie:
      return TIES[tie]
    elif number > tie:
      return TIES[tie] + "-" + human(number - tie)

@p.p
def main():
  letters = 0
  for number in range(1, 1001):
    as_str = human(number)
    as_letters = as_str.replace(" ", "").replace("-", "")
    letters += len(as_letters)
  print(letters)

if __name__ == "__main__":
  p.main()
