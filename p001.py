#!/usr/bin/env python3

import p

@p.p
def main():
  fizzbuzz = 0
  for i in range(1, 1000):
    if (i % 3) == 0 or (i % 5) == 0:
      fizzbuzz += i
  print(fizzbuzz)

if __name__ == "__main__":
  p.main()
